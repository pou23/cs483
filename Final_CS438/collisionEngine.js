class OBB {
    constructor(center, width, height, rotation) {
        this.center = center;     // Center of the OBB
        this.width = width;       // Width of the OBB
        this.height = height;     // Height of the OBB
        this.rotation = rotation; // Rotation angle of the OBB (in radians)
    }

    isPointInside(point) {
        const cosAngle = Math.cos(-this.rotation);
        const sinAngle = Math.sin(-this.rotation);
        const translatedPoint = {
            x: (point.x - this.center.x) * cosAngle - (point.y - this.center.y) * sinAngle,
            y: (point.x - this.center.x) * sinAngle + (point.y - this.center.y) * cosAngle
        };

        return (
            translatedPoint.x >= -this.width / 2 &&
            translatedPoint.x <= this.width / 2 &&
            translatedPoint.y >= -this.height / 2 &&
            translatedPoint.y <= this.height / 2
        );
    }

    // Function to check collision between two OBBs
    static checkCollision(obb1, obb2) {
        const vertices1 = obb1.getVertices();
        const vertices2 = obb2.getVertices();

        for (let i = 0; i < 2; i++) {
            const axis = { x: Math.cos(obb1.rotation + i * Math.PI / 2), y: Math.sin(obb1.rotation + i * Math.PI / 2) };

            let min1 = Infinity, max1 = -Infinity, min2 = Infinity, max2 = -Infinity;
            for (const vertex of vertices1) {
                const projected = vertex.x * axis.x + vertex.y * axis.y;
                min1 = Math.min(min1, projected);
                max1 = Math.max(max1, projected);
            }
            for (const vertex of vertices2) {
                const projected = vertex.x * axis.x + vertex.y * axis.y;
                min2 = Math.min(min2, projected);
                max2 = Math.max(max2, projected);
            }

            if (max1 < min2 || max2 < min1) {
                return false;
            }
        }

        return true;
    }

    // Function to get the vertices of the OBB
    getVertices() {
        const cosAngle = Math.cos(this.rotation);
        const sinAngle = Math.sin(this.rotation);
        const halfWidth = this.width / 2;
        const halfHeight = this.height / 2;

        return [
            { x: this.center.x + halfWidth * cosAngle - halfHeight * sinAngle, y: this.center.y + halfWidth * sinAngle + halfHeight * cosAngle },
            { x: this.center.x - halfWidth * cosAngle - halfHeight * sinAngle, y: this.center.y - halfWidth * sinAngle + halfHeight * cosAngle },
            { x: this.center.x - halfWidth * cosAngle + halfHeight * sinAngle, y: this.center.y - halfWidth * sinAngle - halfHeight * cosAngle },
            { x: this.center.x + halfWidth * cosAngle + halfHeight * sinAngle, y: this.center.y + halfWidth * sinAngle - halfHeight * cosAngle }
        ];
    }
}

// Function to check collision between character and rotating object
function checkCollisionWithRotatingObject() {
    // Create OBB instances for character and rotating object
    const characterOBB = new OBB({ x: CharacterPosition[0] + CharacterPosition[2] / 2, y: CharacterPosition[1] + CharacterPosition[3] / 2 }, CharacterPosition[2], CharacterPosition[3], 0);
    const rotatingObjectOBB = new OBB(rotatingObject.center, rotatingObject.size, rotatingObject.size, rotatingObject.rotation);

    // Check collision between OBBs
    return OBB.checkCollision(characterOBB, rotatingObjectOBB);
}

let collisionCounter = 0; // Initialize collision counter

function checkCollisionWithBox(character, box) {
    // Basic AABB collision detection for character (assuming box shape) and a box entity
    const collision = character[0] < box.x + box.width &&
                      character[0] + character[2] > box.x &&
                      character[1] < box.y + box.height &&
                      character[1] + character[3] > box.y;

    if (collision) {
        // Increment collision counter and update the HTML element
        collisionCounter++;
        document.getElementById('collisionCounter').textContent = `Collisions: ${collisionCounter}`;
    }
    

    return collision;
}



function checkCollisionWithCircle(character, circle) {
    // Checking collision with a circle involves a bit more geometry
    // Find the closest point to the circle within the rectangle
    const closestX = Math.max(character[0], Math.min(circle.x, character[0] + character[2]));
    const closestY = Math.max(character[1], Math.min(circle.y, character[1] + character[3]));
    // Calculate the distance between this point and the circle's center
    const distanceX = circle.x - closestX;
    const distanceY = circle.y - closestY;
    const distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
    // If the distance is less than the circle's radius, there's a collision
    return distance < circle.radius;
}
function rotateSquare(cx, cy, halfSize, angle) {
    // Calculate the four vertices of the square
    const vertices = [
        { x: cx - halfSize, y: cy - halfSize },
        { x: cx + halfSize, y: cy - halfSize },
        { x: cx + halfSize, y: cy + halfSize },
        { x: cx - halfSize, y: cy + halfSize }
    ];

    // Rotate each vertex around the center point
    const rotatedVertices = vertices.map(vertex => {
        // Translate the vertex to the origin
        const translatedX = vertex.x - cx;
        const translatedY = vertex.y - cy;
        // Rotate the vertex
        const rotatedX = translatedX * Math.cos(angle) - translatedY * Math.sin(angle);
        const rotatedY = translatedX * Math.sin(angle) + translatedY * Math.cos(angle);
        // Translate the vertex back to its original position
        return {
            x: rotatedX + cx,
            y: rotatedY + cy
        };
    });

    return rotatedVertices;
}