let CharacterPosition;
let canvas;
let ctx;
let endGame = false;
let entities = []; // Declare entities array in the global scope

let rotatingObject = {
    center: { x: 500, y: 200 }, // Center of the rotating object
    size: 50,                    // Size of the rotating square
    rotation: 0,                 // Initial rotation angle of the rotating object (in radians)
    rotationSpeed: 0.01,         // Rotation speed of the rotating object
    color: "blue"                // Color of the rotating object
};



let physicsEngine = new PhysicsEngine();

function handleCreateScreen(){
    let errorMsg = document.getElementById("error");
    errorMsg.textContent = "";

    //let height = document.getElementById("height").value;
    //let width = document.getElementById("width").value;

    let height = 700;
    let width = 800;

    if (height.length == 0 || width.length == 0 ){
        errorMsg.textContent = "Enter valid height or width";
        return;
    }

    let location = document.getElementById("CanvasLocation");
    let canv = document.createElement("canvas");
    canv.height = height;
    canv.width = width;
    canv.id = "myCanvas";
    canv.style.border = "1px solid #000";
    location.append(canv);

    canvas = document.getElementById("myCanvas");
    ctx = canvas.getContext("2d");
    ctx.fillStyle = "black";
    ctx.fillRect(0, parseInt(height)/2, parseInt(width), parseInt(height)/2);
    CharacterPosition = [0, (parseInt(height)/2 - 15), 40, 15];
    ctx.fillStyle = "red";
    ctx.fillRect(CharacterPosition[0], CharacterPosition[1], CharacterPosition[2], CharacterPosition[3]);

    const boxHeight = 20;
    const boxWidth = 30;
    const minX = 0; // Minimum x coordinate
    const maxX = canvas.width - boxWidth; // Maximum x coordinate, considering the width of the box
    const minY = canvas.height/2 - boxHeight ; // Minimum y coordinate
    const maxY = canvas.height/2 - boxHeight - 60; // Maximum y coordinate, considering the height of the box

    // Generate random x and y coordinates within the specified range
    const randomX = Math.random() * (maxX - minX) + minX;
    const randomY = Math.random() * (maxY - minY) + minY;

    entities = [ // Assign entities to the global array
        {type: 'boxes', x: Math.random() * (maxX - minX) + minX, y: Math.random() * (maxY - minY) + minY, width: 30, height: 20, trig: -1, lowerBound: 300, boundx: 1000, upperBound: 50,velocity: [0, 0]}, // Box entity
        {type: 'boxes', x: Math.random() * (maxX - minX) + minX, y: Math.random() * (maxY - minY) + minY, width: 30, height: 20,velocity: [0, 0]},
        {type: 'boxes', x: Math.random() * (maxX - minX) + minX, y: Math.random() * (maxY - minY) + minY, width: 30, height: 20,velocity: [0, 0]},
        {type: 'boxes', x: Math.random() * (maxX - minX) + minX, y: Math.random() * (maxY - minY) + minY, width: 30, height: 20,velocity: [0, 0]},
        //{type: 'box', x: 600, y: 650, width: 30, height: 50,velocity: [0, 0]},
        {type: 'boxes', x: Math.random() * (maxX - minX) + minX, y: Math.random() * (maxY - minY) + minY, width: 30, height: 20,velocity: [0, 0]},
        {type: 'boxes', x: Math.random() * (maxX - minX) + minX, y: Math.random() * (maxY - minY) + minY, width: 30, height: 20,velocity: [0, 0]},
        {type: 'boxes', x: Math.random() * (maxX - minX) + minX, y: Math.random() * (maxY - minY) + minY, width: 30, height: 20,velocity: [0, 0]},
        {type: 'boxes', x: Math.random() * (maxX - minX) + minX, y: Math.random() * (maxY - minY) + minY, width: 30, height: 20,velocity: [0, 0]},
        {type: 'boxes', x: Math.random() * (maxX - minX) + minX, y: Math.random() * (maxY - minY) + minY, width: 30, height: 20,velocity: [0, 0]}

    ];

    // Add character to physics engine
    let character = {
        position: CharacterPosition.slice(0),
        velocity: [0, 0],
        size: [40, 15],
        onGround: true,
        collidedWithOBB: false,
        
    };
    physicsEngine.addCharacter(character);

    entities.forEach(entity => {
        physicsEngine.addObject(entity);
    });
    render();
}
let  collisionCount = 0;
function render(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "black";
    ctx.fillRect(0, parseInt(canvas.height)/2, parseInt(canvas.width), parseInt(canvas.height)/2);

    ctx.fillStyle = "white";
    ctx.font = "20px Arial";
    //ctx.fillText("Collisions with box: " + collisionCount, 10, 30);


    const size = rotatingObject.size;
    const halfSize = size / 2;
    const boundingBox = {
        x: rotatingObject.center.x - halfSize,
        y: rotatingObject.center.y - halfSize,
        width: size,
        height: size
    };
    ctx.clearRect(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height);

    // Update CharacterPosition based on the character's position from the physics engine
    CharacterPosition = [
        physicsEngine.character.position[0],
        physicsEngine.character.position[1],
        physicsEngine.character.size[0],
        physicsEngine.character.size[1]
    ];

    

    ctx.fillStyle = "red";
    ctx.fillRect(CharacterPosition[0], CharacterPosition[1], CharacterPosition[2], CharacterPosition[3]);
    physicsEngine.update(1); // Assuming a timeStep of 1 for simplicity


    rotatingObject.rotation += rotatingObject.rotationSpeed;
    const rotatedVertices = rotateSquare(rotatingObject.center.x, rotatingObject.center.y, halfSize, rotatingObject.rotation);

    ctx.fillStyle = rotatingObject.color;
    ctx.beginPath();
    ctx.moveTo(rotatedVertices[0].x, rotatedVertices[0].y);
    for (let i = 1; i < rotatedVertices.length; i++) {
        ctx.lineTo(rotatedVertices[i].x, rotatedVertices[i].y);
    }
    ctx.closePath();
    ctx.fill();

    // Check for collision between character and rotating object
    const collisionDetected = checkCollisionWithRotatingObject();

    // If collision is detected, handle it
    if (collisionDetected) {
        console.log(collisionDetected)
        console.log('Collision with oBB box!')
        physicsEngine.character.velocity = [0, 0];
        physicsEngine.character.position[1] = 680;
        physicsEngine.character.collidedWithOBB = true;
        sliding = false;
        physicsEngine.character.onGround = true;
    }

    let fallingCircleCount = 0;

    if (physicsEngine.character.position[0]< 0) {
        // Adjust the character's position to prevent it from going beyond the left wall
        while (fallingCircleCount < 10) {
            // Introduce a delay of 0.5 seconds for each falling circle
            setTimeout(() => {
                const randomColor = '#' + Math.floor(Math.random()*16777215).toString(16);
                entities.push({ type: 'circle', x: Math.random() * canvas.width, y: 0, radius: 20, velocity: [0, 5], color: randomColor });
            }, fallingCircleCount * 500); // Delay is in milliseconds, so 0.5 seconds is 500 milliseconds
            fallingCircleCount++;
        }
        
        physicsEngine.character.onGround = true;
        physicsEngine.character.collidedWithOBB = false;
        physicsEngine.character.velocity = [0, 0];
        physicsEngine.character.position= [5, canvas.height / 2 - physicsEngine.character.size[1]];
        
    }
    entities.forEach(entity => {
        if (entity.type === 'box') {
            ctx.fillStyle = "blue";
            ctx.fillRect(entity.x, entity.y, entity.width, entity.height);

        }
        if (entity.type === 'boxes') {
            ctx.fillStyle = "green";
            ctx.fillRect(entity.x, entity.y, entity.width, entity.height);

        } 
        if (entity.type === 'circle') {
            entity.velocity[1] += physicsEngine.gravity;
            // Update the position of the circle
            entity.x += entity.velocity[0];
            entity.y += entity.velocity[1];

            // Check for collision with the bottom of the screen
            if (entity.y + entity.radius >= parseInt(canvas.height)/2) {
                entity.velocity[1] *= physicsEngine.bouncingCoeff; 
                entity.velocity[0] *= physicsEngine.friction_coefficient;
                entity.y = parseInt(canvas.height)/2 - entity.radius;
            }


            ctx.beginPath();
            ctx.fillStyle = entity.color;
            ctx.arc(entity.x, entity.y, entity.radius, 0, Math.PI * 2);
            ctx.fill();

            // Check and handle circle collision
            if (checkCollisionWithCircle(CharacterPosition, entity)) {
                console.log('Collision with circle!');
                return;
                // Handle circle collision
            }
        }
    });

    if (endGame){
        return;
    }

    requestAnimationFrame(render);
}

document.addEventListener("keydown", handleKeyPress);
document.addEventListener("keyup", handleKeyRelease);

document.addEventListener("keydown", handleCharacter);


