class PhysicsEngine {
    constructor() {
        this.maxvelocity = 10;
        this.jummp = -50;
        this.gravity =9.8; // m/s^2
        this.acceleration = 2;
        this.decceleration = -2;
        this.friction_coefficient = 0.5;
        this.objects = [];
        this.character = null; // Character object
        this.applyFriction = false;
        this.bouncingCoeff = -0.8;
    }

    addObject(obj) {
        this.objects.push(obj);
    }

    addCharacter(character) {
        this.character = character;
    }

    update(timeStep) {
        for (let obj of this.objects) {
            if (obj.updatePosition) {
                updatePosition(obj, this.acceleration, this.gravity, this.maxvelocity, timeStep);
                if (obj.onGround && obj.velocity[1] > 0) {
                    obj.onGround = false;
                }

                if (obj.position[1] > canvas.height / 2 - obj.size[1]) {
                    obj.position[1] = canvas.height / 2 - obj.size[1];
                    obj.velocity[1] = 0;
                    obj.onGround = true;
                }

            }
        }

         // Update character
         if (this.character) {
            if (this.character.onGround) {
                applyFriction(this.character);
            }
            updatePosition(this.character, this.acceleration, this.gravity, this.maxvelocity, timeStep);

            if (this.character.onGround && this.character.velocity[1] > 0) {
                this.character.onGround = false;
            }

            for (let obj of this.objects) {
                if (obj.type === 'boxes'){
                    if (checkCollisionWithBox(this.character.position, obj)){
                        console.log('Collision with box!')
                        this.character.position[1] = obj.y - this.character.size[1];
                        this.character.velocity[1] = canvas.height/2 
                        this.character.onGround = true;
                    }
                }
              
            }
        }
    }

}

function updatePosition(obj, acceleration, gravity, maxvelocity, timeStep) {
    if (physicsEngine.characterVelocityLeft){

        if (obj.velocity[0] > -maxvelocity) {
            obj.velocity[0] -= acceleration * timeStep;
        }
    }
    else if (physicsEngine.characterVelocityRight){
        if (obj.velocity[0]< maxvelocity){
            obj.velocity[0] += acceleration * timeStep;
        }
    }

    obj.velocity[1] += gravity * timeStep;
    obj.position[0] += obj.velocity[0] * timeStep;
    obj.position[1] += obj.velocity[1] * timeStep;

    if (obj.position[1] > canvas.height / 2 - obj.size[1]) {
        if (!obj.collidedWithOBB) { 
            obj.position[1] = canvas.height / 2 - obj.size[1];
        } else { 
            obj.position[1] = canvas.height - obj.size[1];
        }
        
        obj.velocity[1] = 0;
        obj.onGround = true;
    }
}


function applyFriction(obj) {
    obj.velocity[0] *= physicsEngine.friction_coefficient;

}

function handleCharacter(event) {
    if (event.key == "ArrowRight") {

        physicsEngine.characterVelocityRight = true;
    } 

    else if (event.key == "ArrowLeft") {
        physicsEngine.characterVelocityLeft = true;
    } 

    else if (event.key == "ArrowUp") {
        // Apply upward velocity for the jump only if the character is on the ground
        physicsEngine.character.velocity[1] = physicsEngine.jummp; 
        physicsEngine.character.onGround = false; 
    }

    else if (event.key == "q") {
        endGame = true;
    }

}

function handleKeyPress(event) {
    if (event.key === "f") {
        this.applyFriction = true;
    }
}

function handleKeyRelease(event) {
    if (event.key === "f") {
        this.applyFriction = false;
    } 
    physicsEngine.characterVelocityRight = false;
    physicsEngine.characterVelocityLeft = false;
    
}

